const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const fs = require("fs");

// port
app.listen(port, () => {
  console.log("Server listening on port " + port);
});

// middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// database
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/node-demo");

// connect html
app.use("/", (req, res) => {
  res.sendFile(__dirname + "/login.html");
});

// mendefinisikan string
const nameSchema = new mongoose.Schema({
  username: String,
  password: String,
});
const user = mongoose.model("user", nameSchema);

// middleware
app.post("/addname", (req, res, next) => {});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post("/addname", (req, res) => {
  const myData = new User(req.body);
  myData
    .save()
    .then((item) => {
      res.send("item saved to database");
    })
    .catch((err) => {
      res.status(400).send("unable to save to database");
    });
});

// connections
app.post("/login", (req, res) => {
  const { username, password } = req.body;

  const [loggedInUser] = users.filter(
    (user) => user.username === username && user.password === password
  );

  if (loggedInUser) {
    fs.writeFileSync(
      `${path.dirname(__dirname)}${path.sep}data${path.sep}loggedInUser.json`,
      JSON.stringify(loggedInUser)
    );

    res.redirect("/");
  } else {
    res.redirect("/public/html/login.html");
  }
});
