const express = require("express");
const posts = require("./db/users.json");
const app = express();

app.get("/db/users/:id", (req, res) => {
  const post = posts.find((i) => i.id === +req.params.id);
  res.status(200).json(posts);
});

app.listen(3000, () => {
  console.log("server on port 3000!");
});
